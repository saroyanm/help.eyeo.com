# This file is part of help.eyeo.com.
# Copyright [C] 2016 Eyeo GmbH
#
# help.eyeo.com is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# [at your option] any later version.
#
# help.eyeo.com is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with help.eyeo.com.  If not, see <http://www.gnu.org/licenses/>.

browsers = [
  {
    'id': 'chrome',
    'name': 'Chrome'
  },
  {
    'id': 'firefox',
    'name': 'Firefox'
  },
  {
    'id': 'ios',
    'name': 'iOS Safari'
  },
  {
    'id': 'safari',
    'name': 'Safari'
  },
  {
    'id': 'msie',
    'name': 'Internet Explorer'
  },
  {
    'id': 'msedge',
    'name': 'Microsoft Edge'
  },
  {
    'id': 'opera',
    'name': 'Opera'
  },
  {
    'id': 'maxthon',
    'name': 'Maxthon'
  },
  {
    'id': 'samsungBrowser',
    'name': 'Samsung Internet'
  },
  {
    'id': 'yandexbrowser',
    'name': 'Yandex.Browser'
  }
]
