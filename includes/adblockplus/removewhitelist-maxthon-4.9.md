1. Click the **Adblock Plus** icon and select **Adblock Plus Options**.
<br>The *Adblock Plus Options* window opens.
2. Select the **Whitelisted domains** tab.
3. If a website that you do not want to view ads on appears in the box, select the website and click **Remove selected**.
4. Click **Done**.
