1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
<br>The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Clear the check box labeled **Allow Acceptable Ads**.
4. Close the tab.