1. From the Maxthon sidebar, click the **gear** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and click **Options**.
<br>The *Adblock Plus Options* window opens.
4. Select the *Whitelisted domains* tab.
5. If a website that you do not want to view ads on appears in the box, select the website and click **Remove selected**.
6. Click **Done**.
