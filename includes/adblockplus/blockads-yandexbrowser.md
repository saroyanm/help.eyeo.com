1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
4. From the *General* tab, scroll to the Acceptable Ads section.
5. Clear the check box labeled **Allow Acceptable Ads**.
6. Close the tab.
