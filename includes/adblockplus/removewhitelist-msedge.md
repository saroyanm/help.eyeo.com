1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
<br>The *Adblock Plus Settings* tab opens.
2. Select the **Whitelisted websites** tab.
3. If a website that you do not want to view ads on is listed, click the **Trash** icon next to the website.
4. Close the tab.