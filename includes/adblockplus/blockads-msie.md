<section class="platform-msie" markdown="1">
## Internet Explorer

1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Settings**.
<aside class="alert info" markdown="1">**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.</aside>
The *Adblock Plus Options* tab opens.
2. Clear the check box labeled **Allow some nonintrusive advertising**.
3. Close the tab.
</section>
