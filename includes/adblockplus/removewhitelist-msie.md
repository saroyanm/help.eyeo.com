1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Settings**.
<aside class="alert info" markdown="1">**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.</aside>
The *Adblock Plus Options* tab opens.
2. Click **Manage**.
3. If a website that you do not want to view ads on appears in the box, select the website and click **Remove selected**.
4. Close the tab.
