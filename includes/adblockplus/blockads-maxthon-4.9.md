1. Click the **Adblock Plus** icon and select **Adblock Plus Options**.
<br>The *Adblock Plus Options* window opens.
2. Clear the check box labeled **Allow some nonintrusive advertising**.
3. Click **Done**.
