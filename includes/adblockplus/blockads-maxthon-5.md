1. From the Maxthon sidebar, click the **Adblock Plus** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and click **Options**.
<br>The *Adblock Plus Options* window opens.
4. Clear the check box labeled **Allow some nonintrusive advertising**.
5. Click **Done**.
