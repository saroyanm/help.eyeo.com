title=View blocked and blockable items
description=Remove rules that you previously set or view items that can be blocked with Adblock Plus.
template=article
product_id=abp
category=Customization & Settings

Adblock Plus allows you to customize on-page actions depending on the page you're viewing. Follow the steps below if you'd like to block an item, add an exception rule, remove a rule that you previously set, or if you'd simply like to find out which items on a page can be blocked.

<section class="platform-chrome" markdown="1">
## Chrome

### How to view a blocked item

1. Click the **Chrome menu** icon.
2. Hover over **More Tools** and select **Developer Tools**.
3. Click the **>>** icon in the upper right corner and select **Adblock Plus**.

### How to block an item

1. Follow the steps above.
2. Hover over the item you want to block and click **Block item**.

### How to add an exception OR unblock an item

1. Follow the steps under *How to view a blocked item*.
2. From the drop-down menu, select **blocked**.
3. Hover over the item you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

### How to view a blocked item

1. Click the **Firefox menu** icon.
2. Click **Web Developer**.
3. Click **Inspector**.
4. Select the **Adblock Plus** tab.

### How to block an item

1. Follow the steps above.
2. Hover over the item you want to block and click **Block**.

### How to add an exception OR unblock an item

1. Follow the steps under *How to view a blocked item*.
2. From the drop-down menu, select **blocked**.
3. Hover over the item you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>