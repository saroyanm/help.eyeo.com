title=Add a language
description=Change the language that you normally browse websites in.
template=article
product_id=abp
category=Customization & Settings

You can enhance Adblock Plus by changing the language that you normally browse websites in.

<aside class="alert info" markdown="1">**Tip**: As a recommendation, you should avoid adding too many languages to Adblock Plus. Adding too many languages slows down the ad blocker and, therefore, your browsing.</aside>

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Language* section.
3. Do one of the following:
    - Change the default browsing language by clicking **Change** and selecting a new language from the list.
    - Add an additional browsing language by clicking **Add a language** and selecting a language from the list.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Language* section.
3. Do one of the following:
    - Change the default browsing language by clicking **Change** and selecting a new language from the list.
    - Add an additional browsing language by clicking **Add a language** and selecting a language from the list.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Language* section.
3. Do one of the following:
    - Change the default browsing language by clicking **Change** and selecting a new language from the list.
    - Add an additional browsing language by clicking **Add a language** and selecting a language from the list.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Language* section.
3. Do one of the following:
    - Change the default browsing language by clicking **Change** and selecting a new language from the list.
    - Add an additional browsing language by clicking **Add a language** and selecting a language from the list.
4. Close the tab.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
<br>The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Language* section.
3. Do one of the following:
    - Change the default browsing language by clicking **Change** and selecting a new language from the list.
    - Add an additional browsing language by clicking **Add a language** and selecting a language from the list.
4. Close the tab.
</section>