title=Adblock Plus breaks the websites I visit
description=In rare instances, Adblock Plus may prevent some websites from functioning correctly. This is typically not a bug but rather a problem with the filters (or filter subscription).
template=article
product_id=abp
category=Troubleshooting & Reporting
popular=true

In rare instances, Adblock Plus may prevent some websites from functioning correctly. This is typically not a bug but rather a problem with the filters (or filter subscription).

## Temporarily disable Adblock Plus on a website

A filter list may be directing Adblock Plus to block something that should be allowed. To verify that the issue lies within the filter list, temporarily disable Adblock Plus on the website that you are experiencing the issue. If the issue is fixed after temporarily disabling Adblock Plus, you can be certain that the problem is within the filters.

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Enabled on this site**.
Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-msie" markdown="1">
### Internet Explorer

1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Disable on [website]**.
<aside class="alert info" markdown="1">**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.</aside>
Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-maxthon" markdown="1">
### Maxthon 5 (Windows only)

1. From the Maxthon sidebar, click the **gear** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and clear the check box labeled **Enabled**.
<br>Adblock Plus is now disabled.
4. Close the tab.

### Maxthon 4.9 (Windows only)

1. From the Maxthon toolbar, click the **Adblock Plus** icon and select **Block ads on this website**.
<br>Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-safari" markdown="1">
### Safari

1. From the Safari toolbar, click the **Adblock Plus** icon and select **Enabled on this site**.
<br>Adblock Plus is now disabled on the website you are on.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
Adblock Plus is now disabled on the website you are on.
</section>

## Update the filter list(s)

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-ios" markdown="1">
### Adblock Plus for iOS

1. Open the Adblock Plus for iOS app.
2. Tap the **Tools** icon.
3. Tap **Update Filter lists**.
4. Wait 10-20 seconds for the lists to update.
<br>The update is successful if the current date and time appears below *Update filter lists*.
5. Close the app.
</section>

<section class="platform-maxthon" markdown="1">
### Maxthon 5 (Windows only)

1. From the Maxthon sidebar, click the **Adblock Plus** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and click **Options**.
<br>The *Adblock Plus Options* window opens.
4. Click **Update now**.
<br>The update is successful if the current date and time appears next to the list.
5. Click **Done**.

### Maxthon 4.9 (Windows only)

1. Click the **Adblock Plus** icon and select **Adblock Plus Options**.
<br>The *Adblock Plus Options* window opens.
2. Click the **Update Now** button.
<br>The update is successful if the current date and time appears next to the list.
3. Click **Done**.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-samsungBrowser" markdown="1">
### Adblock Plus for Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **Configure your filter lists**.
3. Clear the check box next to the filter you want to update.
4. Tap the back button to refresh.
5. Tap **Configure your filter lists**.
6. Tap the check box next to the filter you want to update.
7. Close the app.
8. Reopen the app to verify that the filter list has been updated.
<br>The update is successful if the current date and time appears below the list.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

## Report bad filters

If updating the filter list(s) does not work, please contact our Support team at support@adblockplus.org.
