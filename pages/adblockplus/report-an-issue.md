title=Report an issue via the Issue reporter
description=Report issues on webpages you visit with the Issue reporter, available for Chrome, Firefox and Opera.
template=article
product_id=abp
category=Troubleshooting & Reporting

The Issue reporter allows users to easily report issues like unblocked ads and missing content. If you're a Chrome, Firefox or Opera user, you need Adblock Plus 3.0.3 or higher to use the Issue reporter. [Check your Adblock Plus version](adblockplus/check-abp-version)

**Note**: The Issue reporter shouldn't be used to report issues with Adblock Plus itself (e.g. not being able to block elements on the page, trouble whitelisting websites, adding or updating filter lists, or any other issues with Adblock Plus settings). To report these types of issues, please contact our Support team at support@adblockplus.org.

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select **Report issue**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Issue reporter* tab opens.</aside>
2. Select the appropriate issue type.
3. Click **Continue**.
4. Highlight the issue you are experiencing and click **Continue**.
5. Enter your email address in the *Email* field.
<aside class="alert warning" markdown="1">**Note**: If you want to submit your issue anonymously, select the check box labeled **Anonymous submission**.</aside>
6. Optionally, enter a comment in the *Comment* field.
<aside class="alert info" markdown="1">**Tip**: Filter hosts may not be able reply to your issue directly, so please provide any additional information that will help make the problem reproducible.</aside>
7. Click **Send report**.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Report issue**.
<br>The *Issue reporter* tab opens.
2. Select the appropriate issue type.
3. Click **Continue**.
4. Enter your email address in the *Email* field.
<aside class="alert warning" markdown="1">**Note**: If you want to submit your issue anonymously, select the check box labeled **Anonymous submission**.</aside>
5. Optionally, enter a comment in the *Comment* field.
<aside class="alert info" markdown="1">**Tip**: Filter hosts may not be able reply to your issue directly, so please provide any additional information that will help make the problem reproducible.</aside>
6. Click **Send report**.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select **Report issue**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Issue reporter* tab opens.</aside>
2. Select the appropriate issue type.
3. Click **Continue**.
4. Highlight the issue you are experiencing and click **Continue**.
5. Enter your email address in the *Email* field.
<aside class="alert warning" markdown="1">**Note**: If you want to submit your issue anonymously, select the check box labeled **Anonymous submission**.</aside>
6. Optionally, enter a comment in the *Comment* field.
<aside class="alert info" markdown="1">**Tip**: Filter hosts may not be able reply to your issue directly, so please provide any additional information that will help make the problem reproducible.</aside>
7. Click **Send report**.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select **Report issue**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Issue reporter* tab opens.</aside>
2. Select the appropriate issue type.
3. Click **Continue**.
4. Highlight the issue you are experiencing and click **Continue**.
5. Enter your email address in the *Email* field.
<aside class="alert warning" markdown="1">**Note**: If you want to submit your issue anonymously, select the check box labeled **Anonymous submission**.</aside>
6. Optionally, enter a comment in the *Comment* field.
<aside class="alert info" markdown="1">**Tip**: Filter hosts may not be able reply to your issue directly, so please provide any additional information that will help make the problem reproducible.</aside>
7. Click **Send report**.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select **Report issue**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Issue reporter* tab opens.</aside>
2. Select the appropriate issue type.
3. Click **Continue**.
4. Highlight the issue you are experiencing and click **Continue**.
5. Enter your email address in the *Email* field.
<aside class="alert warning" markdown="1">**Note**: If you want to submit your issue anonymously, select the check box labeled **Anonymous submission**.</aside>
6. Optionally, enter a comment in the *Comment* field.
<aside class="alert info" markdown="1">**Tip**: Filter hosts may not be able reply to your issue directly, so please provide any additional information that will help make the problem reproducible.</aside>
7. Click **Send report**.
</section>