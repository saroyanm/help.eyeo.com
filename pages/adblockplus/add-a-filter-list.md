title=Add a filter list
description=A default filter list based on your browser's language settings is automatically activated when you install Adblock Plus. Add additional filter lists to block unwanted items, or to block ads on websites that you read in other languages.
template=article
product_id=abp
category=Customization & Settings

Adblock Plus requires filter lists to block ads. A default filter list based on your browser's language settings is automatically activated when you install Adblock Plus. It blocks ads from the most popular (often English language-based) websites. However, it does not block ads on less popular national websites. For example, if you live in Germany you should subscribe to the national German filter list. This filter list is detected as "EasyList Germany + EasyList".

An overview of actively maintained filter lists can be found [here](https://adblockplus.org/en/subscriptions).

<aside class="alert info" markdown="1">**Tip**: As a recommendation, you should avoid adding too many filter lists to Adblock Plus. Adding too many filter lists slows down the ad blocker and, therefore, your browsing.</aside>

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Add a new filter list**.
<br>The *Add a filter list* window opens.
4. Enter the filter list title in the *Filter list name* field.
5. Enter the filter list URL in the *Filter list URL* field.
6. Click **Add a filter list**.
<br>The add is successful if *Just now* appears next to the list.
7. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Add a new filter list**.
<br>The *Add a filter list* window opens.
4. Enter the filter list title in the *Filter list name* field.
5. Enter the filter list URL in the *Filter list URL* field.
6. Click **Add a filter list**.
<br>The add is successful if *Just now* appears next to the list.
7. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
<br>The *Adblock Plus Options* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Add a new filter list**.
<br>The *Add a filter list* window opens.
4. Enter the filter list title in the *Filter list name* field.
5. Enter the filter list URL in the *Filter list URL* field.
6. Click **Add a filter list**.
<br>The add is successful if *Just now* appears next to the list.
7. Close the tab.
</section>>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Settings**.
<aside class="alert info" markdown="1">**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.</aside>
The *Adblock Plus Options* tab opens.
2. Select a filter list from the drop-down menu.
3. Close the tab.
</section>

<section class="platform-maxthon" markdown="1">
## Maxthon 5 (Windows only)

1. From the Maxthon sidebar, click the **Adblock Plus** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and click **Options**.
<br>The *Adblock Plus Options* window opens.
4. Click **Add filter subscription**.
5. Select a filter list from the drop-down menu.
6. Click **Add**.
<br>The add is successful if the current date and time appears next to the list.
7. Click **Done**.

## Maxthon 4.9 (Windows only)

1. Click the **Adblock Plus** icon and select **Adblock Plus Options**.
<br>The *Adblock Plus Options* window opens.
2. Click **Add filter subscription**.
3. Select a filter list from the drop-down menu.
4. Click **Add**.
5. Verify that the filter list update status appears as **Last update (Date) (Time)**.
6. Click **Done**.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Add a new filter list**.
<br>The *Add a filter list* window opens.
4. Enter the filter list title in the *Filter list name* field.
5. Enter the filter list URL in the *Filter list URL* field.
6. Click **Add a filter list**.
<br>The add is successful if *Just now* appears next to the list.
7. Close the tab.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **Configure your filter lists**.
3. From the *Other Languages* section, select the filter list you want to subscribe to.
4. Tap the back button to refresh.
5. Close the app.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Add a new filter list**.
<br>The *Add a filter list* window opens.
4. Enter the filter list URL in the *Filter list URL* field.
5. Click **Add a filter list**.
<br>The add is successful if *Just now* appears next to the list.
6. Close the tab.
</section>
