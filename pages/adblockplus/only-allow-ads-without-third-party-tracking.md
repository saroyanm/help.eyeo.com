title=Only allow ads without third-party tracking
description=Acceptable Ads without third-party tracking are ads that comply with the Acceptable Ads criteria and that do not allow third-party entities to track any of your browsing behavior. These are ads that comply with Do Not Track, and/or ads which are served by the domain which is wholly owned by the same company.
template=article
product_id=abp
category=Customization & Settings

Acceptable Ads without third-party tracking are ads that comply with the Acceptable Ads criteria and that do not allow third-party entities to track any of your browsing behavior. These are ads that comply with Do Not Track, and / or ads which are served by the domain which is wholly owned by the same company.

<aside class="alert info" markdown="1">**Important**: You must have both the **Allow Acceptable Ads** option and **Do Not Track (DNT)** enabled.</aside>

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Select the check box labeled **Only allow ads without third-party tracking**.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Select the check box labeled **Only allow ads without third-party tracking**.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Select the check box labeled **Only allow ads without third-party tracking**.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Select the check box labeled **Only allow ads without third-party tracking**.
4. Close the tab.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, scroll to the *Acceptable Ads* section.
3. Select the check box labeled **Only allow ads without third-party tracking**.
4. Close the tab.
</section>