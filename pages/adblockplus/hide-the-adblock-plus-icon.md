title=Hide or show the Adblock Plus icon
description=Hide or show the Adblock Plus icon on your browser's toolbar.
template=article
product_id=abp
category=Customization & Settings

You can easily hide the Adblock Plus icon if you do not want it displayed on your toolbar.

<section class="platform-chrome" markdown="1">
## Chrome

### How to hide the icon...

1. From the Chrome toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Hide in Chrome menu**.
<br>The icon is removed from the toolbar.

### How to show the icon...

1. Click the **Chrome menu** icon.
2. Right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Show in toolbar** (Windows) / **Keep in Toolbar** (Mac).
<br>The icon now appears on the toolbar.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

### How to hide the icon...

1. From the Firefox toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Remove from Toolbar**.
<br>The icon is removed from the toolbar.

### How to show the icon...

1. Right-click (Windows) / control-click (Mac) the Firefox toolbar and select **Customize**.
<br>The *Customize Firefox* tab opens.
2. Select the **Adblock Plus** icon and drag it to the toolbar.
3. Drop the icon in your preferred location.
<aside class="alert info" markdown="1">**Tip**: You can reorder the toolbar icons while the *Customize Firefox* tab is open.</aside>
4. Click **Done**.
<br>The icon now appears on the toolbar.
</section>

<section class="platform-opera" markdown="1">
## Opera

### How to hide the icon...

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manange extension**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and select the check box labeled **Hide from toolbar**.
<br>The icon is removed from the toolbar.

### How to show the icon...

1. Click the **Opera** icon, hover over **Extensions** and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and clear the check box labeled **Hide from toolbar**.
<br>The icon now appears on the toolbar.
3. Close the tab.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Hide button**.
<br>The icon is removed from the toolbar.

### How to show the icon...

1. Click the **Yandex Browser menu** icon and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and click **More details**.
3. Click **Show button**.
<br>The icon now appears on the toolbar.
4. Close the tab.
</section>
