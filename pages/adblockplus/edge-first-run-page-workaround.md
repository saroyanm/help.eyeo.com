title=Stop first run page from opening each time Edge is launched
description=Some Adblock Plus for Edge users see the first run page each time Edge is launched.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

Some Edge users are experiencing an issue where the same page (first run page) opens each time their browser is launched. This issue occurs because Adblock Plus settings are lost when the user clears their browsing history. We're currently working with Microsoft to resolve this issue and in the meantime suggest the following workaround:

1. Open Edge.
2. Click **More(...)** and select **Settings**.
3. Clear the check box labeled **Cookies and saved website data**.