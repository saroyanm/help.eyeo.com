title=What are Acceptable Ads?
description=Acceptable Ads are respectful, nonintrusive ads that do not interfere with your browsing. By allowing these ads. website owners who abide by the Acceptable Ads criteria continue to earn ad revenue.
template=article
product_id=abp
category=Popular Topics
popular=true
hide_browser_selector=true

Acceptable Ads are respectful ads that do not disrupt the content you are viewing.

The Acceptable Ads criteria was developed in 2011 as a joint effort between Adblock Plus and its community of users. In 2017, eyeo GmbH (the company behind Adblock Plus) handed the criteria over to an [independent committee](https://acceptableads.com/en/committee/). When you install Adblock Plus and Adblock Browser, all nonintrusive ads are displayed by default. You can, however, disable this feature at any time and hide all ads. To do this, refer to [this article](adblock-plus/block-all-ads.md).

To view the entire Acceptable Ads criteria, visit the [Acceptable Ads criteria page](https://adblockplus.org/en/acceptable-ads#criteria).
