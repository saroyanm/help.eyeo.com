title=Check your Adblock Plus version
description=Learn how to find which version of Adblock Plus you are using.
template=article
product_id=abp
category=Customization & Settings

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Click **About Adblock Plus** in the lower left corner.
The *About Adblock Plus* window opens, displaying the version number.
3. Click **Close**.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
The *Adblock Plus Settings* tab opens.
2. Click **About Adblock Plus** in the lower left corner.
The *About Adblock Plus* window opens, displaying the version number.
3. Click **Close**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Click **About Adblock Plus** in the lower left corner.
The *About Adblock Plus* window opens, displaying the version number.
3. Click **Close**.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Click **About Adblock Plus** in the lower left corner.
The *About Adblock Plus* window opens, displaying the version number.
3. Click **Close**.
</section>

<section class="platform-safari" markdown="1">
## Safari

1. From the Mac menu bar, click **Safari** and select **Preferences**.
The *Preferences* window opens.
2. Select the *Extensions* tab.
The version number appears next to the Adblock Plus icon.
3. Close the window.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Open your device's **Settings** and tap **Apps**.
<aside class="alert warning" markdown="1">**Note**: Settings vary depending on the device. "Settings" and "Apps" might be labeled differently.</aside>
2. Tap **Adblock Plus for Samsung Internet**.
3. Scroll to locate the version number.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Click **About Adblock Plus** in the lower left corner.
The *About Adblock Plus* window opens, displaying the version number.
3. Click **Close**.
</section>