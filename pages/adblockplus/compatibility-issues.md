title=Compatibility issues
description=You must be on the current version of your browser or platform for Adblock Plus to work properly.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

If Adblock Plus or Adblock Browser cannot be installed due to compatibility issues, please verify that Adblock Plus or Adblock Browser supports your [platform's version](https://adblockplus.org/requirements).

<aside class="alert info" markdown="1">**Important**: Adblock Plus and Adblock Browser cannot function on older browser / platform versions that are no longer supported.</aside>

To ensure that your platform is up-to-date, visit:

- [Desktop browsers](https://whatbrowser.org/)
- [Android](https://whatismyandroidversion.com/)
- [iOS](https://support.apple.com/en-us/HT201685)
