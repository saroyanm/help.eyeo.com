title=I still see ads
description=If you still see ads after installing Adblock Plus, you may need to adjust your settings or verify your filter lists.
template=article
product_id=abp
category=Troubleshooting & Reporting
popular=true
include_heading_level=h3

Follow the suggestions below if you still see ads after installing Adblock Plus.

## Make sure Adblock Plus is enabled

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extensions**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
2. Verify that Adblock Plus is toggled **on**.
3. Close the tab.

</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Firefox menu** icon and select **Add-ons**.
2. Select **Extensions** from the left menu.
3. Locate Adblock Plus and verify that it is not disabled.
4. If it is not disabled, click the **Adblock Plus** icon and verify that **Disable everywhere** is **NOT** selected.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
5. Close the tab.
</section>

<section class="platform-msie" markdown="1">
### Internet Explorer

1. Click **Tools** in the upper right corner of the Internet Explorer toolbar.
<br>The *Manage Add-ons* window opens.
2. Select **Manage Add-ons**.
3. Locate and select Adblock Plus.
4. Verify that the status reads **Enabled**.
5. Close the window.
</section>

<section class="platform-maxthon" markdown="1">
### Maxthon 5 (Windows only)

1. From the Maxthon sidebar, click the **gear** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the **Addons** tab.
3. Locate Adblock Plus and verify that the **Enabled** check box is selected.
3. Close the tab.

### Maxthon 4.9 (Windows only)

1. Click the **Maxthon menu** icon in the upper right corner of the toolbar and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and verify that the **Enabled** check box is selected.
3. Close the tab.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
2. Locate Adblock Plus and verify that it is enabled.
3. Close the tab.
</section>

<section class="platform-safari" markdown="1">
### Safari

1. From the Mac menu bar, click **Safari** and select **Preferences**.
<br>The *Preferences* window opens.
2. Select the **Extensions** tab.
3. From the left menu, locate Adblock Plus and verify that the check box next to the Adblock Plus icon is selected.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Yandex Browser menu** icon and select **Extensions**.
<br>The *Extensions* tab opens.
2. Select **Extensions**.
3. Locate Adblock Plus and verify that it is toggled on.
4. Close the tab.
</section>

## Manually update all filter lists

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
<br>The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

## Verify that you are using the correct filter list

Adblock Plus requires filter lists to block ads. A default filter list based on your browser's language settings is automatically activated when you install Adblock Plus. It blocks ads from the most popular (often English language-based) websites. However, it does not block ads on less popular national websites. For example, if you live in Germany you should subscribe to the national German filter list.

An overview of actively maintained filter lists can be found [here](https://adblockplus.org/en/subscriptions).

<aside class="alert info" markdown="1">**Tip**: As a recommendation, you should avoid adding too many filter lists to Adblock Plus. Adding too many filter lists slows down the ad blocker and, therefore, your browsing.</aside>

## Make sure the website is not whitelisted

<? include adblockplus/removewhitelist ?>

## Turn off Acceptable Ads

Some nonintrusive ads are displayed by default. If you want to block all ads, this feature must be turned off. Please note that by turning off Acceptable Ads, website owners and content creators that abide by the Acceptable Ads criteria will lose ad revenue.

<? include adblockplus/blockads ?>

## Check to see if your computer is infected with adware/malware

### What is adware?

Adware is a type of malware, short for "malicious software." It's a term generally used for software installed on your computer that is designed to infiltrate or damage a computer system without your consent. In many cases you obtained this software without knowing it, as these applications are often bundled in other software installers.

For information about detecting and removing malware, visit our [adware information page](https://adblockplus.org/adware).

If you still see ads after following these recommendations, please report the website(s) in the [forum](https://adblockplus.org/forum/).
