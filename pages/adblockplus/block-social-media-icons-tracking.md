title=Block social media icons tracking
description=Use Adblock Plus to help block social media sites from tracking your online activity.
template=article
product_id=abp
category=Customization & Settings

Social media icons track your online browsing activities. Even if you never click one of these icons, a profile based on your browsing habits can be created and sent to various social network servers. Use Adblock Plus to stop social media sites from tracking your online activity. This means all social media icons will be hidden.

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block social media icons tracking**.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and select **Options**.
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block social media icons tracking**.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block social media icons tracking**.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block social media icons tracking**.
4. Close the tab.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **More blocking options**.
3. Tap the check box labeled **Disable social media buttons**.
4. Tap the back button.
5. Close the app.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the **gear** icon in the upper right corner.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block social media icons tracking**.
4. Close the tab.
</section>
