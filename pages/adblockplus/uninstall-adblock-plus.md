title=Uninstall Adblock Plus
description=You can temporarily disable Adblock Plus rather than uninstall it. Still want to uninstall Adblock Plus? Learn more here.
template=article
product_id=abp
category=Installation

<section class="platform-chrome" markdown="1">
## Chrome

<aside class="alert info" markdown="1">**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.</aside>

1. From the Chrome toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extensions**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
2. Click the toggle **off**.
3. Close the tab.

### How to uninstall Adblock Plus...

1. From the Chrome toolbar, click the **Chrome menu** icon, hover over **More Tools** and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and click **Remove**.
<br>A dialog box opens.
3. Click **Remove**.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Edge menu** icon and select **Extensions**.
2. Hover over Adblock Plus and click the **gear** icon.
3. Scroll to the bottom of the dialog and click **Uninstall**.
<br>A dialog box opens.
4. Click **OK**.
5. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.

1. Click the **Firefox menu** icon and select **Add-ons**.
<br>The *Add-ons Manager* tab opens.
2. Select **Extensions** from the left menu.
3. Locate Adblock Plus and click **Disable**.
4. Close the tab.

### How to uninstall Adblock Plus...

1. Click the **Firefox menu** icon and select **Add-ons**.
<br>The *Add-ons Manager* tab opens.
2. Select **Extensions** from the left menu.
3. Locate Adblock Plus and click **Remove**.
4. Close the tab.
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.

1. Click the **Adblock Plus** icon.
<aside class="alert info" markdown="1">**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.</aside>
2. Select **Disable everywhere**.

### How to uninstall Adblock Plus...

Adblock Plus must be uninstalled via the Control Panel.

#### Windows 8 and 10...
1. From the Desktop, right-click **Start** and select **Control Panel**.
2. Click **Uninstall a Program**.
3. Select Adblock Plus from the installed program list and click **Uninstall**.
<br>A dialog box opens.
4. Click **Yes**.
5. Close the window.

#### Windows 7...

1. Click **Start** and select **Control Panel**.
2. Click **Uninstall a Program**.
3. Select Adblock Plus from the installed program list and click **Uninstall**.
<br>A dialog box opens.
4. Click **Yes**.
5. Close the window.
</section>

<section class="platform-ios" markdown="1">
## Adblock Plus for iOS

1. Long-tap the Adblock Plus icon until the screen starts to wiggle.
2. Tap the **X** in the upper-left corner of the Adblock Plus icon.
</section>

<section class="platform-maxthon" markdown="1">
## Maxthon 5 (Windows only)

1. From the Maxthon sidebar, click the **gear** icon and select **Extension Manager**.
<br>The *Maxthon Settings* tab opens.
2. Select the *Addons* tab.
3. Locate Adblock Plus and click the **Trash** icon.
<br>A dialog box opens.
4. Click **OK**.
5. Close the tab.

## Maxthon 4.9 (Windows only)

<aside class="alert warning" markdown="1">**Note**: Because Adblock Plus is a feature of Maxthon 4.9, it cannot be uninstalled. You can, however, disable it.</aside>

1. Click the **Maxthon menu** icon in the upper right corner of the toolbar and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and clear its check box.
<br> Adblock Plus is now disabled.
3. Close the tab.
</section>

<section class="platform-opera" markdown="1">
## Opera

**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and click **Disable**.
3. Close the tab.

### How to uninstall Adblock Plus...

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
3. Locate Adblock Plus and click the **X** in the upper-right corner.
<br>A dialog box opens.
4. Click **OK**.
5. Close the tab.
</section>

<section class="platform-safari" markdown="1">
## Safari

**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.

1. From the Mac menu bar, click **Safari** and select **Preferences**.
<br>The *Preferences* window opens.
2. Select the *Extensions* tab.
3. From the left menu, clear the check box next to the Adblock Plus icon and the ABP Control Panel check box.
4. Close the window.

### How to uninstall Adblock Plus...

1. Follow the steps above to disable Adblock Plus. Do not close the *Preferences* window.
2. Locate Adblock Plus and click **Uninstall**.
<br>A dialog box opens.
3. Click **Show in Finder**.
<br>The Finder opens.
4. Locate the Adblock Plus app and move it to the **Trash**.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

### Android 7 and 8...

1. Tap and hold the Adblock Plus for Samsung Internet icon.
2. When the bubble window appears above the icon, tap **Uninstall**.
3. Tap **OK**.

### Android 6 and below...

1. Long-press the Adblock Plus for Samsung Internet icon.
2. Drag and drop it to the **Uninstall button** at the top of the screen.
3. Tap **Yes**.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

**Tip**: To disable, rather than uninstall Adblock Plus, follow the steps below.

1. From the Yandex Browser toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extensions**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
2. Locate Adblock Plus and click **On**.
3. Close the tab.

### How to uninstall Adblock Plus...

1. From the Yandex Browser toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extensions**.
<aside class="alert info" markdown="1">**Tip**: Refer to [this article](adblockplus/hide-the-adblock-plus-icon) if you do not see the Adblock Plus icon.</aside>
The *Extensions* tab opens.
2. Locate Adblock Plus and click **More details**.
3. Click **Delete**.
<br>A dialog box opens.
4. Click **Remove**.
5. Close the tab.
</section>
