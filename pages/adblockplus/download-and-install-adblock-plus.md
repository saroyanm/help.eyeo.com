title=Download and install Adblock Plus
description=Adblock Plus is available for Chrome, Firefox, Internet Explorer, Maxthon, Opera, Safari and Yandex.Browser desktop browsers, as well as a content blocker on iOS and Samsung Internet.
template=article
product_id=abp
category=Installation

<section class="platform-chrome" markdown="1">
## Chrome

There are two ways to install Adblock Plus for Chrome.

### From the Adblock Plus website...

1. Open Chrome and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Chrome**.
<br>A dialog box opens.
4. Click **Add extension**.

### From the Chrome Web Store...

1. Click [here](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb) to open Adblock Plus in the Chrome Web Store.
2. Click **Add to Chrome**.
<br>A dialog box opens.
3. Click **Add extension**.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. Open Edge and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Microsoft Edge**.
<br>The Microsoft Store opens.
3. Click **Get**.
<br>A new window opens.
4. Click **Install**.
5. Click **Launch**
<br>A dialog opens.
6. Click **Turn it on**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Open Firefox and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Firefox**.
<br>A dialog box opens.
3. Click **Allow**.
<br>A dialog box opens.
4. Click **Add**.
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. Open Internet Explorer and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Internet Explorer**.
<br>A dialog box opens at the bottom of the browser.
3. Click the arrow and select **Save and run**.
<br>A Windows dialog box opens asking for permission to install Adblock Plus.
4. Click **Yes**.
<br>The *Install Wizard* opens.
<br>A dialog box opens asking to close Internet Explorer.
5. Click **Yes**.
<br>A dialog box opens asking if the *Install Wizard* can close Internet Explorer.
6. Click **Yes**.
7. Click **Next**.
8. Click **Install**.
9. After installation is complete, click **Finish**.
10. Open Internet Explorer.
<br>A dialog box opens prompting you to enable Adblock Plus.
11. Click **Enable**.
12. Close and reopen Internet Explorer.
</section>

<section class="platform-maxthon" markdown="1">
## Maxthon 5 (Windows only)

<aside class="alert info" markdown="1">**Important**: In the latest version of Maxthon 5, you must select the check box labeled **Install Adblock Plus** from the Maxthon 5 install screen.</aside>

</section>

<section class="platform-ios" markdown="1">
## Adblock Plus for iOS

1. Tap [here](https://itunes.apple.com/us/app/adblock-plus-abp-remove-ads/id1028871868?mt=8) to open Adblock Plus for iOS in the App Store.
2. Tap **Install**.
<aside class="alert info" markdown="1">**Important**: After you install Adblock Plus for iOS, you are prompted to enable ad blocking.</aside>
To do this:
    1. Open iOS Settings.
    2. Tap **Safari**.
    3. Tap **Content Blockers**.
    4. Turn on Adblock Plus.
    5. Exit iOS Settings.
</section>

<section class="platform-opera" markdown="1">
## Opera

Adblock Plus for Opera must be installed via Opera add-ons.

1. Open Opera and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Opera**.
<br>The *Opera add-ons* page opens.
3. Click **Add to Opera**.
</section>

<section class="platform-safari" markdown="1">
## Safari

1. Open Safari and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Safari**.
<br>The App Store opens.
3. Click the **Download** icon in the upper left corner.
4. After the file downloads, click **Open**.
<br>The *Adblock Plus onboarding* window opens.
5. Click **Launch Safari Preferences**.
<br>The *Extensions* window opens.
6. Select the check box labeled **ABP**.
7. Select the check box labeled **ABP Control Panel**.
8. Close the *Extensions* window.
9. Click **Finish**.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Tap [here](https://play.google.com/store/apps/details?id=org.adblockplus.adblockplussbrowser&hl=en) to open Adblock Plus for Samsung Internet in the Play Store.
2. Tap **Install**.
<aside class="alert info" markdown="1">**Important**: After you install Adblock Plus for Samsung Internet, you are prompted to enable Adblock Plus.</aside>
To do this:
<br>Samsung Internet 6.0...
    1. Open the Samsung Internet app.
    2. Tap the **menu** icon and select **Extensions**.
    3. Tap **Content blockers**.
    4. Turn on Adblock Plus for Samsung Internet.
    5. Exit Settings.
<br>Samsung Internet 5.0...
    1. Open the Samsung Internet app.
    2. Tap the **menu** icon and select **Extensions**.
    4. Tap **Block content**.
    5. Enable **Content Blocking** and tap Adblock Plus for Samsung Internet.
    6. Exit Settings.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Open Yandex Browser and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Yandex Browser**.
<br>A dialog box opens.
3. Click **Add extension**.
</section>
