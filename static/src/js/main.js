/*!
 * This file is part of help.eyeo.com.
 * Copyright (C) 2017-present eyeo GmbH
 *
 * help.eyeo.com is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * help.eyeo.com is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with help.eyeo.com.  If not, see <http://www.gnu.org/licenses/>.
 */
(function()
{
  document.addEventListener("DOMContentLoaded", function()
  {

    /**************************************************************************
     * General
     **************************************************************************/

    // Change html class name from "no-js" to "js"
    document.documentElement.className = "js";

    /**************************************************************************
     * CustomSelect
     **************************************************************************/

    function CustomSelect(select)
    {
      this.select = select;
      this.close();
      this.select
          .addEventListener("click", this._onClick.bind(this), false);
      this.select
          .addEventListener("focusout", this._onFocusOut.bind(this), false);
    }

    CustomSelect.prototype._onFocusOut = function()
    {
      // setTimeout to allow document.activeElement 
      // to move to newly focused element
      setTimeout(function()
      {
        var newFocus = document.activeElement;
        
        if (newFocus
              .classList.contains("custom-select-selected") ||
            newFocus
              .classList.contains("custom-select-option") ||
            newFocus
              .parentElement
              .classList.contains("custom-select-option"))
        {
          return;
        }

        this.close();
        
      }.bind(this), 1);
    }

    CustomSelect.prototype._onClick = function(event)
    {
      if (event.target.closest(".custom-select-selected"))
      {
        var options = this.select.querySelector(".custom-select-options");
        if (options.getAttribute("aria-hidden") == "true")
        {
          this.open();
        }
        else
        {
          this.close();
        }
      }
    }

    CustomSelect.prototype.open = function()
    {
      this.select
        .querySelector(".custom-select-selected")
        .setAttribute("aria-expanded", "true");

      this.select
        .querySelector(".custom-select-options")
        .removeAttribute("aria-hidden");
    }

    CustomSelect.prototype.close = function()
    {
      this.select
        .querySelector(".custom-select-selected")
        .setAttribute("aria-expanded", "false");

    this.select
        .querySelector(".custom-select-options")
        .setAttribute("aria-hidden", "true");
    }

    var languageSelect = document.getElementById("language-select");
    if (languageSelect) new CustomSelect(languageSelect);

    /**************************************************************************
     * Accordion
     **************************************************************************/

    function Accordion(accordion)
    {
      this.accordion = accordion;
      
      var accordionButtons = this.accordion.getElementsByClassName('accordion-toggle-button');
      for (var i = 0; i < accordionButtons.length; i++)
      {
        // Close all sections except the first
        if (i !== 0)
        {
          accordionButtons[i].setAttribute("aria-expanded", "false");
          document
            .getElementById( accordionButtons[i].getAttribute("aria-controls") )
            .setAttribute("hidden", "true");
        }
      }

      this.accordion
        .addEventListener("click", this._onClick.bind(this), false);
      this.accordion
        .addEventListener("keydown", this._onKeyDown.bind(this), false);
    }

    Accordion.prototype.toggleSection = function(clickedButton)
    {
      // Hide currently expanded section
      var expandedButton = this.accordion.querySelector("button[aria-expanded='true']");
      if (expandedButton)
      {
        expandedButton.setAttribute("aria-expanded", "false");
        document
          .getElementById( expandedButton.getAttribute("aria-controls") )
          .setAttribute("hidden", "true");
      }

      // If currently expanded section is clicked
      if (expandedButton === clickedButton) return;

      // Expand new section
      clickedButton.setAttribute("aria-expanded", "true");
      document
        .getElementById( clickedButton.getAttribute("aria-controls") )
        .removeAttribute("hidden");
    }

    Accordion.prototype.focusNextSection = function()
    {
      var currentFocus = document.activeElement;
      var nextheading = currentFocus.parentElement.nextElementSibling.nextElementSibling;

      if (nextheading)
      {
        nextheading // .accordion-heading
          .firstElementChild // .accordion-toggle-button
          .focus();
      }
      else
      {
        this.accordion
          .firstElementChild // .accordion-heading
          .firstElementChild // .accordion-toggle-button
          .focus();
      }
    }

    Accordion.prototype.focusPrevSection = function()
    {
      var currentFocus = document.activeElement;
      var prevAccordionBody = currentFocus.parentElement.previousElementSibling;

      if (prevAccordionBody)
      {
        prevAccordionBody // .accordion-body
          .previousElementSibling // .accordion-heading
          .firstElementChild // .accordion-toggle-button
          .focus();
      }
      else
      {
        this.accordion
          .lastElementChild // .accordion-body
          .previousElementSibling // .accordion-heading
          .firstElementChild // .accordion-toggle-button
          .focus();
      }
    }

    Accordion.prototype._onKeyDown = function(event)
    {
      if (!event.target.classList.contains("accordion-toggle-button")) return;

      if (event.key == "ArrowUp" || event.keyCode == 38)
      {
        this.focusPrevSection();
      }
      else if (event.key == "ArrowDown" || event.keyCode == 40)
      {
        this.focusNextSection();
      }
    }

    Accordion.prototype._onClick = function(event)
    {
      if (!event.target.classList.contains("accordion-toggle-button")) return;
      
      this.toggleSection(event.target);
    }

    var productTopicsAccordion = document.getElementById('product-topics-accordion');
    if (productTopicsAccordion)
    {
      new Accordion(productTopicsAccordion);
    }

    /**************************************************************************
     * BrowserSelect
     **************************************************************************/

    function BrowserSelect(select)
    {
      this.select = select;
      CustomSelect.apply(this, [this.select]);
      this.noContentMessage = document.getElementById("no-content-for-platform-notice");

      this.BROWSER_STORAGE_KEY = "BROWSER";
      this.BROWSER_AUTODETECTED_STORAGE_KEY = "BROWSER_AUTODETECTED";
      this.SUPPORTED_BROWSERS = ["chrome", "opera", "samsungBrowser", 
                                 "yandexbrowser", "maxthon", "msie", 
                                 "msedge", "firefox", "ios", "safari"];
      this.DEFAULT_BROWSER = "chrome";
      
      this.setCurrentArticleSupportedBrowsers();
      
      this.select
        .addEventListener("click", this._onClickOrKeyDown.bind(this), false);
      
      this.select
        .addEventListener("keydown", this._onClickOrKeyDown.bind(this), false);

      this.noContentMessage
        .addEventListener("click", this._onClickNoContentMessage.bind(this), false);

      try
      {
        var storedBrowser = localStorage.getItem(this.BROWSER_STORAGE_KEY);
      }
      catch(err) {}

      if (storedBrowser) this.selectOption(storedBrowser);
      else this.detectBrowser();
    }

    BrowserSelect.prototype = Object.create(CustomSelect.prototype);
    BrowserSelect.prototype.constructor = BrowserSelect;

    BrowserSelect.prototype.setCurrentArticleSupportedBrowsers = function()
    {
      for (var i = 0; i < this.SUPPORTED_BROWSERS.length; i++)
      {
        var supportedBrowser = this.SUPPORTED_BROWSERS[i];
        if (!document.querySelector(".platform-"+supportedBrowser))
        {
          this.noContentMessage
              .querySelector("[data-value='" + supportedBrowser + "']")
              .setAttribute("hidden", "true");
        }
      }
    };

    BrowserSelect.prototype.detectBrowser = function()
    {
      for (var i = 0; i < this.SUPPORTED_BROWSERS.length; i++)
      {
        var supportedBrowser = this.SUPPORTED_BROWSERS[i];
        if (bowser[supportedBrowser])
        {
          try
          {
            localStorage.setItem(this.BROWSER_AUTODETECTED_STORAGE_KEY, "true");
          }
          catch(err) {}
          
          return this.selectOption(supportedBrowser);
        }
      }

      this.selectOption(this.DEFAULT_BROWSER);  
    };

    BrowserSelect.prototype.selectOption = function(browser)
    {
      try
      {
        localStorage.setItem(this.BROWSER_STORAGE_KEY, browser);
      }
      catch(err) {}      

      // Change body class
      var bodyClassList = Array.prototype.slice.call(document.body.classList);
      for (var i = 0; i < bodyClassList.length; i++)
      {
        if (bodyClassList[i].indexOf("ua-") > -1)
        {
          document.body.classList.remove(bodyClassList[i]);
        }
      }
      document.body.classList.add("ua-" + browser);

      // Check selected option
      var selectedItem = this.select
                             .querySelector("[data-value='" + browser + "']");
      selectedItem.setAttribute("aria-checked", "true");

      // Set selected option
      var selectedOption = selectedItem.innerHTML;

      try
      {
        if (localStorage.getItem(this.BROWSER_AUTODETECTED_STORAGE_KEY))
        {
          var autodetected = document
                                .getElementById("browser-select-autodetected")
                                .innerHTML;
          selectedOption += "<span class='muted'>(" + autodetected + ")</span>";
        }
      }
      catch(err) {}

      this.select
          .querySelector(".custom-select-selected")
          .innerHTML = selectedOption;

      if (document.querySelector(".platform-" + browser))
      {
        this.noContentMessage.setAttribute("hidden", "true");
      }
      else
      {
        this.noContentMessage.removeAttribute("hidden");
      }
    };

    BrowserSelect.prototype._onClickOrKeyDown = function(event)
    {
      var option = event.target.closest(".custom-select-option");
      if (!option) return;

      var IS_ENTER_KEY = event.key == "Enter" || event.keyCode == 13;
      if (event.keyCode && !IS_ENTER_KEY) return;

      try
      {
        localStorage.removeItem(this.BROWSER_AUTODETECTED_STORAGE_KEY);
      }
      catch(err) {}

      // Uncheck previously checked option
      this.select
          .querySelector("[aria-checked='true']")
          .setAttribute("aria-checked", "false");

      this.selectOption(option.getAttribute("data-value"));

      this.close();
    };

    BrowserSelect.prototype._onClickNoContentMessage = function(event)
    {
      if (event.target.tagName != "BUTTON") return;
      this.selectOption(event.target.parentElement.getAttribute("data-value"));
    };

    var browserSelect = document.getElementById("browser-select");
    if (browserSelect)
    {
      new BrowserSelect(browserSelect);
    }

  }, false);
}());