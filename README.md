# help.eyeo.com

Source code and content of [help.eyeo.com](https://help.eyeo.com).

## Getting started

**1. Install [CMS](https://github.com/adblockplus/cms)**

This static site is generated using the Adblock Plus CMS. For more information 
and usage instructions see [CMS documentation](https://github.com/adblockplus/cms/blob/master/README.md#content-structure).

**2. Install [Gulp.js](https://gulpjs.com/) globally**

Gulp is required to build the static assets from the source files.

```
npm install gulp-cli -g
```

**3. Install dependencies**

```
npm install
```

**4. Build static assets**

```
gulp
```

**5. Run the test server**

Start the cms test server

```
python path/to/cms/runserver.py
```

## License

- Source code: [GPL-3.0](https://www.gnu.org/licenses/gpl.html)
- Content: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
